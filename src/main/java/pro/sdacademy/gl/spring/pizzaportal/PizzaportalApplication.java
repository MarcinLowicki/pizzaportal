package pro.sdacademy.gl.spring.pizzaportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzaportalApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaportalApplication.class, args);
	}

}
