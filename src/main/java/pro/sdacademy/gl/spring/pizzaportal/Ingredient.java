package pro.sdacademy.gl.spring.pizzaportal;


import lombok.Data;

@Data
public class Ingredient {

    private Integer id;
    private String name;
    private Integer weight;

    private boolean extended;

}
