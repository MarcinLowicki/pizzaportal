package pro.sdacademy.gl.spring.pizzaportal;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/restaurantList")
public class RestaurantController {

    @GetMapping
    public String getRestaurantList(Model model) {
        model.addAttribute("restaurants", List.of(
                restaurant1(),
                restaurant2(),
                restaurant3()
        ));
        return "restaurantList";
    }

    @GetMapping("/{id}")
    public String getRestaurantPage(@PathVariable("id") int restaurantId,
                                    Model model) {
        model.addAttribute("restaurant", restaurant1());
        model.addAttribute("meals", List.of(
                meal1(),
                meal2()
        ));
        return "restaurantPage";
    }

    private Meal meal1() {
        var meal = new Meal();
        meal.setId(1);
        meal.setName("Burger Drwala");
        meal.setSize(Meal.MealSize.XXL);
        return meal;
    }

    private Meal meal2() {
        var meal = new Meal();
        meal.setId(2);
        meal.setName("Zupa");
        meal.setSize(Meal.MealSize.L);
        return meal;
    }

    private Restaurant restaurant1() {
        var restaurant = new Restaurant();
        restaurant.setId(1);
        restaurant.setName("PATAYA");
        restaurant.setPhone("+32 34756873485");

        Restaurant.Address address = new Restaurant.Address();
        address.setCity("Gliwie");
        address.setStreet("Bankowa");
        address.setStreetNumber(12);
        restaurant.setAddress(address);

        return restaurant;
    }

    private Restaurant restaurant2() {
        var restaurant = new Restaurant();
        restaurant.setId(2);
        restaurant.setName("Dominium");
        restaurant.setPhone("+32 34725672845");

        Restaurant.Address address = new Restaurant.Address();
        address.setCity("Katowice");
        address.setStreet("Szczebrzeszyn");
        address.setStreetNumber(50);
        restaurant.setAddress(address);

        return restaurant;
    }

    private Restaurant restaurant3() {
        var restaurant = new Restaurant();
        restaurant.setId(3);
        restaurant.setName("Mc'Donalds");
        restaurant.setPhone("+32 3847528746");

        Restaurant.Address address = new Restaurant.Address();
        address.setCity("Gdańsk");
        address.setStreet("Westerplatte");
        address.setStreetNumber(1);
        restaurant.setAddress(address);

        return restaurant;
    }

}
