package pro.sdacademy.gl.spring.pizzaportal;

import lombok.Data;
import lombok.Getter;

@Data
public class Meal {
    private Integer id;
    private String name;
    private MealSize size;

    public enum MealSize {
        L("Duże"),
        XL("Bardzo duże"),
        XXL("Bardzo bardzo duże");

        @Getter
        private String description;

        MealSize(String description) {
            this.description = description;
        }
    }
}
