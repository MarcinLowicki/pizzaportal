package pro.sdacademy.gl.spring.pizzaportal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class MealController {

    @GetMapping("/meals/{id}")
    public String getMealPage(@PathVariable("id") int mealId,
                              Model model) {
        Meal meal = new Meal();
        meal.setName("Burger");
        meal.setSize(Meal.MealSize.XL);

        model.addAttribute("meal", meal);
        model.addAttribute("ingredients", List.of(
                ingredient1(),
                ingredient2()
        ));
        return "mealPage";
    }

    private Ingredient ingredient1() {
        var ingredient = new Ingredient();
        ingredient.setId(1);
        ingredient.setName("Cheese");
        ingredient.setWeight(50);
        ingredient.setExtended(true);
        return ingredient;
    }

    private Ingredient ingredient2() {
        var ingredient = new Ingredient();
        ingredient.setId(2);
        ingredient.setName("Tomato");
        ingredient.setWeight(15);
        ingredient.setExtended(false);
        return ingredient;
    }
}


