package pro.sdacademy.gl.spring.pizzaportal;

import lombok.Data;

@Data
public class Restaurant {

    private Integer id;
    private String name;
    private Address address;
    private String phone;

    @Data
    public static class Address{
        private String city;
        private String street;
        private int streetNumber;
    }

}
