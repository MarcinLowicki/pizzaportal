<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h2>Restaurants:</h2>

<table>
    <tr>
        <th>Nazwa</th>
        <th>Adres</th>
        <th>Numer kontaktowy</th>
        <th></th>
    </tr>
    <c:forEach var="restaurant" items="${restaurants}">
        <tr>
            <td>${restaurant.name}</td>
            <td>${restaurant.address.street} ${restaurant.address.streetNumber}</td>
            <td>${restaurant.phone}</td>
            <td><a href="/restaurantList/${restaurant.id}">
                <input type="button" value="Zobacz"/></a></td>
        </tr>
    </c:forEach>
</table>
