<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h2>Dane restauracji:</h2>
${restaurant.name}<br/>
${restaurant.address.street} ${restaurant.address.streetNumber}<br/>
${restaurant.phone}<br/>

<h2>Dania:</h2>
<table>
    <tr>
        <th>Nazwa</th>
        <th>Rozmiar</th>
        <th></th>
    </tr>
    <c:forEach var="meal" items="${meals}">
        <tr>
            <td>${meal.name}</td>
            <td>${meal.size.description}</td>
            <td><a href="/meals/${meal.id}"><input type="button" value="Wybierz"/></a></td>
        </tr>
    </c:forEach>
</table>
