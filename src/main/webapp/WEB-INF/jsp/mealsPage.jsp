<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h2>Potrawa:</h2>
${meal.name} ${meal.size.description}<br/>

<h2>Składniki:</h2>
<table>
    <tr>
        <th>Nazwa</th>
        <th></th>
    </tr>
    <c:forEach var="ingredient" items="${ingredients}">
        <tr>
            <td>${ingredient.name}</td>
            <td><c:if test="${ingredient.extended}">Powiększony</c:if></td>
        </tr>
    </c:forEach>
</table>
